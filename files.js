const PRID = 'files-311816';
const BUCKET = 'files-bucket999';
const {Storage} = require('@google-cloud/storage');
const Firestore = require('@google-cloud/firestore');

const uploadToBucket = (name, mimetype, buffer) => {
	const pr = new Promise((resolve, reject) => {
	const storage = new Storage();
		const bucket = storage.bucket(BUCKET);
		const file = bucket.file(name);

		const stream = file.createWriteStream({
			metadata: {
				contentType: mimetype
			}
		});
		stream.on('error', (err) => {
			console.log('error', err);
		});
		stream.on('finish', (r) => {
			console.log('ready!', name, mimetype);
			resolve({ name :name, mimetype : mimetype});
		});
		stream.end(new Buffer(buffer, 'base64'));
	});
	return pr;
}

const saveToFirebase = (file) => {
	const pr = new Promise((resolve, reject) => {
		const db = new Firestore({projectId : PRID});
		const FILEDB = 'files';
		const fileEntry = {name: file.name, mimetype : file.mimetype};
		db.collection('files').add(fileEntry).then(r => {
			console.log('new entry', r.id);
			resolve(r.id);
		});
	});
	return pr;
};

const getFileFromDb = (id) => {
	const pr = new Promise((resolve, reject) => {
		const db = new Firestore({projectId : PRID});
		db.collection('files').doc(id).get().then(r => {
			if (!r.data()) reject('Can not find this file!');
			console.log('$$$$$', r);
			resolve(r.data());
		});
	});
	return pr;
};

const downloadFileFromBucket = (file) => {
	const pr = new Promise((resolve, reject) => {
		const storage = new Storage();
		const bucket = storage.bucket(BUCKET);
		bucket.file(file.name).download().then(r => {
			resolve(r[0]);
		});
	});
	return pr;
};

const getPublicUrl = (file) => {
	const storage = new Storage();
	const bucket = storage.bucket(BUCKET);
	return bucket.file(file.name).publicUrl();
}

const getLatestUploads = (n = 10) => {
	const pr = new Promise((resolve, reject) => {
		const db = new Firestore({projectId : PRID});
		db.collection('files').limit(n).get().then(r => {
			let idList = [];
			r.forEach(el => {
				idList.push(el.id);
			});
			resolve(idList);
		});
	});
	return pr;
}

module.exports = {
	uploadToBucket : uploadToBucket,
	getFileFromDb : getFileFromDb,
	saveToFirebase : saveToFirebase,
	downloadFileFromBucket : downloadFileFromBucket,
	getPublicUrl : getPublicUrl,
    getLatestUploads : getLatestUploads	
};

