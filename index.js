// GOOGLE_APPLICATION_CREDENTIALS
const files = require('./files');
const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');

const storage = multer.memoryStorage();
const upload = multer({ storage : storage});

const PORT = process.env.PORT || 3000;

const errorHandler = (err, req, res, next) => {
	res.status(500).render('pages/error', {msg: err.toString()});
}

const app = express();
app.set('view engine', 'ejs');
app.use(express.static('static'));
app.use(bodyParser.urlencoded({extended : true}));

app.get('/', (req, res, next) => {
	files.getLatestUploads().then(latest => {
		res.render('pages/index', {latest :  latest} );
	});
});

app.get('/files/:id', (req, res, next) => {
 	files.getFileFromDb(req.params.id)
	.then(file => {
		if (file.mimetype === 'text/plain') {
			files.downloadFileFromBucket(file).then(buffer => {
				res.render('pages/file',
					{buffer : buffer.toString()}).end();
			});
		} else {
			res.render('pages/fileLink', {publicUrl : files.getPublicUrl(file)});
		}
	}).catch(err => {
		next(err);
		return;
	});
});

app.post('/upload', upload.single('myfile'), (req, res, next) => {
	if (!req.file) {
			next('No empty uploads!');
			return;
		}

	files.uploadToBucket('ff-' + Date.now() + '-' + req.file.originalname, 
					req.file.mimetype,
					req.file.buffer)
				.then(files.saveToFirebase)
				.then(id => {
					res.redirect('/files/' + id).end();
				});
});

app.post('/add', (req, res, next) => {
	if (!req.body.text || req.body.text.length == 0) {
		next('You cant upload an empty file.');
		return;
	}
	let myBuffer = Buffer.from(req.body.text);
	files.uploadToBucket('ff-' + Date.now() + '-add.txt', 
					'text/plain',	
					myBuffer)
				.then(files.saveToFirebase)
				.then(id => {
					res.redirect('/files/' + id).end();
				});
})

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
